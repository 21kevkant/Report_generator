import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MantenimientoV implements ActionListener 
{
   JFrame vMantenimiento;

   private JLabel lbl_codigo, lbl_nombre, lbl_apellido, lbl_departamento, lbl_cargo, lbl_venta_mensual, lbl_venta_anual;
   private JTextField tf_codigo, tf_nombre, tf_apellido, tf_departamento, tf_cargo, tf_venta_mensual, tf_venta_anual, tf_limpiar;
   private JButton btn_eliminar, btn_buscar, btn_agregar, btn_modificar, btn_limpiar;
   
   JComboBox<String> jcb_departamento;
   
   Vendedor vendedor;
   Departamento departamento;
    
   MantenimientoV(JFrame x)
   {
      vMantenimiento = x;
      vMantenimiento.getContentPane().removeAll();
      vMantenimiento.getContentPane().setBackground(Color.lightGray);
      vMantenimiento.revalidate();
       
       vendedor = new Vendedor();
       departamento = new Departamento();
       
       btn_buscar = new JButton("Buscar");
       btn_buscar.setBounds(225,50,100,20);
       btn_buscar.addActionListener(this);
       vMantenimiento.add(btn_buscar);
       
       btn_agregar = new JButton("agregar");
       btn_agregar.setBounds(225,75,100,20);
       btn_agregar.addActionListener(this);
       vMantenimiento.add(btn_agregar);
       
       btn_eliminar = new JButton("eliminar");
       btn_eliminar.setBounds(225,100,100,20);
       btn_eliminar.addActionListener(this);
       vMantenimiento.add(btn_eliminar);
       
       btn_modificar = new JButton("modificar");
       btn_modificar.setBounds(225,125,100,20);
       btn_modificar.addActionListener(this);
       vMantenimiento.add(btn_modificar);
       
       btn_limpiar = new JButton("limpiar");
       btn_limpiar.setBounds(250,150,100,20);
       btn_limpiar.addActionListener(this);
       vMantenimiento.add(btn_limpiar);
      
       lbl_codigo = new JLabel("Codigo");
       lbl_codigo.setBounds(50,50,80,20);
       vMantenimiento.add(lbl_codigo);
      
       tf_codigo = new JTextField();
       tf_codigo.setBounds(125,50,80,20);
       vMantenimiento.add(tf_codigo);
      
       lbl_nombre = new JLabel("Nombre");
       lbl_nombre.setBounds(50,75,80,20);
       vMantenimiento.add(lbl_nombre);
      
       tf_nombre = new JTextField();
       tf_nombre.setBounds(125,75,80,20);
       vMantenimiento.add(tf_nombre);
   
       lbl_apellido = new JLabel("Apellido");
       lbl_apellido.setBounds(50,100,80,20);
       vMantenimiento.add(lbl_apellido);
      
       tf_apellido = new JTextField();
       tf_apellido.setBounds(125,100,80,20);
       vMantenimiento.add(tf_apellido);
       
       lbl_cargo = new JLabel("Cargo");
       lbl_cargo.setBounds(50,125,80,20);
       vMantenimiento.add(lbl_cargo);
      
       tf_cargo = new JTextField();
       tf_cargo.setBounds(125,125,80,20);
       vMantenimiento.add(tf_cargo);
      
       lbl_venta_mensual = new JLabel("venta Mensual");
       lbl_venta_mensual.setBounds(50,150,90,20);
       vMantenimiento.add(lbl_venta_mensual);
      
       tf_venta_mensual = new JTextField();
       tf_venta_mensual.setBounds(150,150,80,20);
       vMantenimiento.add(tf_venta_mensual);
       
       lbl_venta_anual = new JLabel("Venta Anual");
       lbl_venta_anual.setBounds(50,175,80,20);
       vMantenimiento.add(lbl_venta_anual);
      
       tf_venta_anual = new JTextField();
       tf_venta_anual.setBounds(150,175,80,20);
       vMantenimiento.add(tf_venta_anual);
       
       lbl_departamento = new JLabel("Departamento");
       lbl_departamento.setBounds(50,200,90,20);
       vMantenimiento.add(lbl_departamento);
      
       jcb_departamento = new JComboBox<String>(departamento.vleer());
       jcb_departamento.setBounds(150,200,120,20);
       vMantenimiento.add(jcb_departamento);
       
       vMantenimiento.repaint();
   }
   
public void actionPerformed(ActionEvent e)
   {
     if (e.getSource() == btn_buscar)
     {
         System.out.println("BUSCADO");
         vendedor.buscar(tf_codigo.getText());
         tf_nombre.setText(vendedor.getNombre());
         tf_apellido.setText(vendedor.getApellido());
         tf_cargo.setText(vendedor.getCargo());
         tf_venta_mensual.setText(String.valueOf(vendedor.getVenta_mensual()));
         tf_venta_anual.setText(String.valueOf(vendedor.getVenta_anual()));
     }
    
     if(e.getSource() == btn_agregar)
     {
         System.out.println("AGREGADO");
         vendedor.setCodigo(tf_codigo.getText());
         vendedor.setNombre(tf_nombre.getText());
         vendedor.setApellido(tf_apellido.getText());
         vendedor.setCargo(tf_cargo.getText());
         vendedor.setVenta_mensual(Integer.parseInt(tf_venta_mensual.getText()));
         vendedor.setVenta_anual(Integer.parseInt(tf_venta_anual.getText()));
         vendedor.agregar();
     }
    
     if(e.getSource() == btn_eliminar)
         {
             System.out.println("ELIMADO");
             vendedor.setCodigo(tf_codigo.getText());
             vendedor.setNombre(tf_nombre.getText());
             vendedor.setApellido(tf_apellido.getText());
             vendedor.setCargo(tf_cargo.getText());
             vendedor.setVenta_mensual(Integer.parseInt(tf_venta_mensual.getText()));
             vendedor.setVenta_anual(Integer.parseInt(tf_venta_anual.getText()));
             vendedor.eliminar();

         }
            
     if(e.getSource() == btn_modificar)
     {
         System.out.println("MODIFICADO");
         vendedor.setCodigo(tf_codigo.getText());
         vendedor.setNombre(tf_nombre.getText());
         vendedor.setApellido(tf_apellido.getText());
         vendedor.setCargo(tf_cargo.getText());
         vendedor.setVenta_mensual(Integer.parseInt(tf_venta_mensual.getText()));
         vendedor.setVenta_anual(Integer.parseInt(tf_venta_anual.getText()));
         vendedor.modificar();

     } 
    
    if (e.getSource() == btn_limpiar)
    {
        System.out.println("LIMPIADO");
        tf_nombre.setText("");
        tf_codigo.setText("");
        tf_apellido.setText("");
        tf_cargo.setText("");
        tf_venta_mensual.setText("");
        tf_venta_anual.setText("");
    }
           
   }
}