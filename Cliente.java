import java.sql.*;
import javax.swing.table.*;

public class Cliente 
{
   String cedula, nombre, apellido, provincia, direccion, telefono;
    int compra_anual;
   
   public void setCedula(String c)
   {
      cedula = c;
   }
   public String getCedula()
   {
      return cedula;
   }
   
   public void setNombre(String n)
   {
      nombre = n;
   }
   public String getNombre()
   {
      return nombre;
   }
   
   public void setApellido(String a)
   {
      apellido = a;
   }
   public String getApellido()
   {
      return apellido;
   }
   public void setProvincia(String p)
   {
      provincia = p;
   }
   public String getProvincia()
   {
      return provincia;
   }
   public void setDireccion(String d)
   {
      direccion = d;
   }
   public String getDireccion()
   {
      return direccion;
   }
       public void setTelefono(String t)
   {
      telefono = t;
   }
   public String getTelefono()
   {
      return telefono;
   }
       public void setCompra_anual(int c_a)
   {
      compra_anual = c_a;
   }
   public int getCompra_anual()
   {
      return compra_anual;
   }

   
   public void buscar(String ced)
   {
      ResultSet rs;
      String sql;
      BD bd = new BD();
      try
      {
         sql = "select * from cl13nt3 where cedula = '"+ced+"'";
         rs = bd.executeQuery(sql);
         if (rs.next())
         {
            cedula = rs.getString("cedula");
            nombre = rs.getString("nombre");
            apellido = rs.getString("apellido");
            direccion = rs.getString("direccion");
            telefono = rs.getString("telefono");
            provincia = rs.getString("provincia");
            compra_anual = Integer.parseInt(rs.getString("compra_anual"));
         }
          else
          {
              System.out.println("no hay resultado");
          }
      }
      catch(Exception e)
      {
         System.out.println("error cliente buscar "+e.toString());
      }
      
   }
    
   public void agregar()
    {
      ResultSet rs;
      String sql;
      BD bd = new BD();
       
       try
       {
            sql = "insert into cl13nt3 (cedula, nombre, apellido, direccion, telefono, provincia, compra_anual) values ('"+cedula+"', '"+nombre+"', '"+apellido+"', '"+direccion+"', '"+telefono+"', 'de', "+compra_anual+")";
           bd.executeUpdate(sql);
           bd.cerrar();
       }
       catch(Exception e)
       {
          System.out.println("Error CLIENTE agregar"+e.toString());
       }
      
    }
    
    public void modificar()
    {
       ResultSet rs;
       String sql;
       BD bd = new BD();
       try
       {
          sql = "update cl13nt3 set nombre = '"+nombre+"', apellido = '"+apellido+"', direccion = '"+direccion+"', telefono = '"+telefono+"', provincia = '"+provincia+"', compra_anual = '"+compra_anual+"' where cedula = '"+cedula+"'";
          bd.executeUpdate(sql);
          bd.cerrar();
       }
       catch(Exception e)
       {
          System.out.println("Error CLIENTE Modificar "+e.toString());
       }
      
    }
    
    public void eliminar()
    {
        ResultSet rs;
       String sql;
       BD bd = new BD();
       try
       {
          sql = "delete from cl13nt3 where cedula = '"+cedula+"'";
          bd.executeUpdate(sql);
          bd.cerrar();
       }
       catch(Exception e)
       {
          System.out.println("Error CLIENTE Eliminar "+e.toString());
       }
    }
    
    public void cargarTabla(DefaultTableModel dtm)
   {
      BD bd = new BD();
      Object[] fila = new Object[7];
      String sql;
      ResultSet rs;
      dtm.addColumn("Cedula");
      dtm.addColumn("Nombre");
      dtm.addColumn("Apellido");
      dtm.addColumn("Provincia");
      dtm.addColumn("direccion");
      dtm.addColumn("telefono");
      dtm.addColumn("compra_anual");
      try
      {
         sql = "select * from cl13nt3, pr0v1nc14 ";
         sql = sql + "where cl13nt3.provincia = pr0v1nc14.codigo";
         rs = bd.executeQuery(sql);
         while (rs.next())
         {
            fila[0] = rs.getString("cedula");
            fila[1] = rs.getString("nombre");
            fila[2] = rs.getString("apellido");
            fila[3] = rs.getString("provincia");
            fila[4] = rs.getString("direccion");
            fila[5] = rs.getString("telefono");
            fila[6] = rs.getString("compra_anual");
            dtm.addRow(fila);
         }
         bd.cerrar();
      }
      catch(Exception e)
      {
         System.out.println("Error CLIENTE cargarTabla"+e.toString());
      }
   }


}