import java.sql.*;
import java.util.*;

public class Departamento
{

   String codigo, nombre;
   List<Departamento> adepartamento = new ArrayList<Departamento>();
   
   public void setCodigo(String s)
   {
      codigo = s;
   }
   public String getCodigo()
   {
      return codigo;
   }
   public void setNombre(String s)
   {
      nombre = s;
   }
   public String getNombre()
   {
      return nombre;
   }
   
   Departamento()
   {
      cargar();
   }
   
   Departamento(int a)
   {
   }
   
   
   public void cargar()
   {
      String sql;
      ResultSet rs;
      BD bd = new BD();
      
      try
      {
         sql = "select * from d3p4rt4m3nt0 order by nombre";
         rs = bd.executeQuery(sql);
         Departamento departamento;
         while(rs.next())
         {
            departamento = new Departamento(1);
            departamento.setCodigo(rs.getString("codigo"));
            departamento.setNombre(rs.getString("nombre"));
            adepartamento.add(departamento);
         }
         bd.cerrar();
      }
      catch(Exception e)
      {
         System.out.println("Error DEPARTAMENTO cargar "+e.toString());
      }
   }

   public String[] vleer()
   {
      int largo = adepartamento.size();
      int i;
      String[] dep = new String[largo];
      for (i=0;i<largo;i++)
         dep[i] = adepartamento.get(i).getNombre();
      return dep;
   }
   
   
}