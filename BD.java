import java.sql.*;

public class BD
{
   String URL, user, pass;
   Connection con;
   Statement stmt;
   ResultSet rs;

   BD()
   {
      URL = "jdbc:mysql://127.0.0.1/t13nd4";
      user = "root";
      pass = "";
   }
   
   public void abrir()
   {
       try
       {
          Class.forName("com.mysql.jdbc.Driver");
          con = DriverManager.getConnection(URL,user,pass);
          stmt = con.createStatement();
       }
       catch (Exception e)
       {
         System.out.println("Error BD abrir"+e.toString());
       }
   
   }
   
   public void cerrar()
   {
      try
      {
         if (rs != null)
            rs.close();
         stmt.close();
         con.close();
      }
      catch(Exception e)
      {
         System.out.println("Error BD cerrar"+e.toString());
      }
   }
   
   
   public ResultSet executeQuery(String sql)
   {
      try
      {
         abrir();
         rs = stmt.executeQuery(sql);
      }
      catch(Exception e)
      {
         System.out.println("Error BD query"+e.toString());
      }
      return rs;
   }
   
   public void executeUpdate(String sql)
   {
      try
      {
         abrir();
         stmt.executeUpdate(sql);
      }
      catch(Exception e)
      {
         System.out.println("Error BD update"+e.toString());
      }

   }
   
   public Connection con()
   {
      return con;
   }
}








