import java.sql.*;
import java.util.*;

public class Provincia
{

   String codigo, nombre;
   List<Provincia> aprovincia = new ArrayList<Provincia>();
   
   public void setCodigo(String s)
   {
      codigo = s;
   }
   public String getCodigo()
   {
      return codigo;
   }
   public void setNombre(String s)
   {
      nombre = s;
   }
   public String getNombre()
   {
      return nombre;
   }
   
   Provincia()
   {
      cargar();
   }
   
   Provincia(int a)
   {
   }
   
   
   public void cargar()
   {
      String sql;
      ResultSet rs;
      BD bd = new BD();
      
      try
      {
         sql = "select * from pr0v1nc14 order by nombre";
         rs = bd.executeQuery(sql);
         Provincia provincia;
         while(rs.next())
         {
            provincia = new Provincia(1);
            provincia.setCodigo(rs.getString("codigo"));
            provincia.setNombre(rs.getString("nombre"));
            aprovincia.add(provincia);
         }
         bd.cerrar();
      }
      catch(Exception e)
      {
         System.out.println("Error provincia cargar "+e.toString());
      }
   }

   public String[] vleer()
   {
      int largo = aprovincia.size();
      int i;
      String[] prov = new String[largo];
      for (i=0;i<largo;i++)
         prov[i] = aprovincia.get(i).getNombre();
      return prov;
   }
}


















