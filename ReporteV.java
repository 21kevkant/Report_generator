import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.awt.event.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.*;
import java.util.*;

public class ReporteV implements ActionListener
{
   JFrame vReporte;
   DefaultTableModel dtm;
   JTable jt_vendedor;
   JScrollPane jsp_vendedor;
   
   JButton btn_reporte;
   
   JRadioButton rb_cedula, rb_nombre, rb_apellido, rb_departamento;
   ButtonGroup bg_orden;
   
   Vendedor vendedor;
    
   ReporteV(JFrame x)
   {
      vReporte = x;
      vReporte.getContentPane().removeAll();
      vendedor = new Vendedor();
      vReporte.revalidate();
      
      dtm = new DefaultTableModel();
      vendedor.cargarTabla(dtm);
      jt_vendedor = new JTable(dtm);
      jsp_vendedor = new JScrollPane(jt_vendedor);
      
      jsp_vendedor.setBounds(50,50,480,100);
      vReporte.add(jsp_vendedor);
      
      btn_reporte = new JButton("Reporte");
      btn_reporte.setBounds(50,185,100,20);
      btn_reporte.addActionListener(this);
      vReporte.add(btn_reporte);
      
      rb_cedula = new JRadioButton("Cedula");
      rb_cedula.setBounds(70,210,80,20);
      vReporte.add(rb_cedula);
      
      rb_nombre = new JRadioButton("Nombre");
      rb_nombre.setBounds(70,235,80,20);
      vReporte.add(rb_nombre);
      
      rb_apellido = new JRadioButton("Apellido");
      rb_apellido.setBounds(70,260,80,20);
      vReporte.add(rb_apellido);
      
      rb_departamento = new JRadioButton("Departamento");
      rb_departamento.setBounds(70,285, 170,20);
      vReporte.add(rb_departamento);
      
      
      bg_orden = new ButtonGroup();
      bg_orden.add(rb_cedula);
      bg_orden.add(rb_nombre);
      bg_orden.add(rb_apellido);
      bg_orden.add(rb_departamento);
      
      vReporte.repaint();
   }
   
public void actionPerformed(ActionEvent e)
   {
     if (e.getSource() == btn_reporte)
      {
         try
         {
            BD bd = new BD();
            bd.abrir();
            Map<String,Object> parametros = new HashMap<String,Object>();
            if (rb_cedula.isSelected())
            {
               parametros.put("orden","cedula");
               parametros.put("titulo","Reporte Cliente Ordenado por Cedula");
            }
            else
               if (rb_nombre.isSelected())
               {
                  parametros.put("orden","nombre");
                  parametros.put("titulo","Reporte Cliente Ordenado por NOMBRE");
               }
               else
                  if (rb_apellido.isSelected())
                  {
                     parametros.put("orden","apellido");
                     parametros.put("titulo","Reporte Cliente Ordenado por APELLIDO");
                  }
            JasperPrint jasperPrint = JasperFillManager.fillReport("Reporte_vendedor.jasper", parametros, bd.con());
            JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
            jasperViewer.setVisible(true);
         }
         catch (Exception e1)
         {
         }
      }
   }
}