import java.sql.*;
import javax.swing.table.*;

public class Vendedor 
{
   String codigo, nombre, apellido, departamento, cargo;
    int venta_mensual, venta_anual;
   
   public void setCodigo(String c)
   {
      codigo = c;
   }
   public String getCodigo()
   {
      return codigo;
   }
   
   public void setNombre(String n)
   {
      nombre = n;
   }
   public String getNombre()
   {
      return nombre;
   }
   
   public void setApellido(String a)
   {
      apellido = a;
   }
   public String getApellido()
   {
      return apellido;
   }
   public void setDepartamento(String d)
   {
      departamento = d;
   }
   public String getDepartamento()
   {
      return departamento;
   }
   public void setCargo(String ca)
   {
      cargo = ca;
   }
   public String getCargo()
   {
      return cargo;
   }
       public void setVenta_mensual(int v_m)
   {
      venta_mensual = v_m;
   }
   public int getVenta_mensual()
   {
      return venta_mensual;
   }
       public void setVenta_anual(int v_a)
   {
      venta_anual = v_a;
   }
   public int getVenta_anual()
   {
      return venta_anual;
   }

   
   public void buscar(String cod)
   {
      ResultSet rs;
      String sql;
      BD bd = new BD();
      try
      {
         sql = "select * from v3nd3d0r where codigo = '"+cod+"'";
         rs = bd.executeQuery(sql);
         if (rs.next())
         {
            codigo = rs.getString("codigo");
            nombre = rs.getString("nombre");
            apellido = rs.getString("apellido");
            cargo = rs.getString("cargo");
            venta_mensual = Integer.parseInt (rs.getString("venta_mensual"));
            departamento = rs.getString("departamento");
            venta_anual = Integer.parseInt(rs.getString("venta_anual"));
             
         }
          else
          {
              System.out.println("no hay resultado");
          }
      }
      catch(Exception e)
      {
         System.out.println("error VENDEDOR buscar "+e.toString());
      }
      
   }
    
   public void agregar()
    {
      ResultSet rs;
      String sql;
      BD bd = new BD();
       
       try
       {
            sql = "insert into v3nd3d0r (codigo, nombre, apellido, cargo, departamento, venta_mensual, venta_anual) values ('"+codigo+"', '"+nombre+"', '"+apellido+"', '"+cargo+"', 'ca', '"+venta_mensual+"', "+venta_anual+")";
           bd.executeUpdate(sql);
           bd.cerrar();
       }
       catch(Exception e)
       {
          System.out.println("Error VENDEDOR agregar"+e.toString());
       }
      
    }
    
    public void modificar()
    {
       ResultSet rs;
       String sql;
       BD bd = new BD();
       try
       {
          sql = "update v3nd3d0r set nombre = '"+nombre+"', apellido = '"+apellido+"', cargo = '"+cargo+"', venta_mensual = '"+venta_mensual+"', departamento = '"+departamento+"', venta_anual = '"+venta_anual+"' where codigo = '"+codigo+"'";
          bd.executeUpdate(sql);
          bd.cerrar();
       }
       catch(Exception e)
       {
          System.out.println("Error VENDEDOR Modificar "+e.toString());
       }
      
    }
    
    public void eliminar()
    {
       ResultSet rs;
       String sql;
       BD bd = new BD();
       try
       {
          sql = "delete from v3nd3d0r where codigo = '"+codigo+"'";
          bd.executeUpdate(sql);
          bd.cerrar();
       }
       catch(Exception e)
       {
          System.out.println("Error VENDEDOR Eliminar "+e.toString());
       }
    }
   
   public void cargarTabla(DefaultTableModel dtm)
   {
      BD bd = new BD();
      Object[] fila = new Object[7];
      String sql;
      ResultSet rs;
      dtm.addColumn("Codigo");
      dtm.addColumn("Nombre");
      dtm.addColumn("Apellido");
      dtm.addColumn("Departamento");
      dtm.addColumn("Cargo");
      dtm.addColumn("venta_mensual");
      dtm.addColumn("venta_anual");
      try
      {
         sql = "select * from v3nd3d0r, d3p4rt4m3nt0 ";
         sql = sql + "where v3nd3d0r.departamento = d3p4rt4m3nt0.codigo";
         rs = bd.executeQuery(sql);
         while (rs.next())
         {
            fila[0] = rs.getString("codigo");
            fila[1] = rs.getString("nombre");
            fila[2] = rs.getString("apellido");
            fila[3] = rs.getString("departamento");
            fila[4] = rs.getString("cargo");
            fila[5] = rs.getString("venta_mensual");
            fila[6] = rs.getString("venta_anual");
            dtm.addRow(fila);
         }
         bd.cerrar();
      }
      catch(Exception e)
      {
         System.out.println("Error VENDEDOR cargarTabla"+e.toString());
      }
   }


}