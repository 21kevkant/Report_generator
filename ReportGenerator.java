import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.util.*;

public class ReportGenerator implements ActionListener
{
   JFrame ventana;
   JMenuBar menuBar;
   JMenu mInicio;
   JMenuItem mPresentacion;
   JMenu mMantenimiento;
   JMenuItem mCliente, mVendedor;
   JMenu mReporte;
   JMenuItem mRCliente, mRVendedor;
   
   public static void main(String[] args)
   {
      new ReportGenerator();
   }
   ReportGenerator()
   {
      ventana = new JFrame("Generador de Reporte");
      ventana.setBounds(100,100,600,500);
      ventana.setLayout(null);
      ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      menuBar = new JMenuBar();
      
      mInicio = new JMenu("Inicio");
        mPresentacion = new JMenuItem("Presentacion");
        mPresentacion.addActionListener(this);
        mInicio.add(mPresentacion);
      menuBar.add(mInicio);
      
      mMantenimiento = new JMenu("Mantenimiento");
        mCliente = new JMenuItem("Mantenimiento Cliente");
        mCliente.addActionListener(this);
        mMantenimiento.add(mCliente);

      mMantenimiento.addSeparator();
      
        mVendedor = new JMenuItem("Mantenimiento Vendedor");
        mVendedor.addActionListener(this);
        mMantenimiento.add(mVendedor);
      
      menuBar.add(mMantenimiento);
      
      mReporte = new JMenu("Reporte");
        mRCliente = new JMenuItem("Reporte Cliente");
        mRCliente.addActionListener(this);
        mReporte.add(mRCliente);
      
      mReporte.addSeparator();
      
        mRVendedor = new JMenuItem("Reporte Vendedor");
        mRVendedor.addActionListener(this);
        mReporte.add(mRVendedor);
      
      menuBar.add(mReporte);
      
      ventana.setJMenuBar(menuBar);
      
      ventana.setVisible(true);
   }
   
   public void actionPerformed(ActionEvent e)
   {
      if (e.getSource() == mPresentacion)
        {
          System.out.println("PRESENTACION");
          new Presentacion(ventana);
        }
        
       if (e.getSource() == mCliente)
         {
           System.out.println("Mantenimiento Cliente");
           new MantenimientoC(ventana);
         }
       if (e.getSource() == mVendedor)
          {
            new MantenimientoV(ventana);
          }
       if (e.getSource() == mRCliente)
          {
            new ReporteC(ventana);
          }
       if (e.getSource() == mRVendedor)
          {
            new ReporteV(ventana);
          }
         
   }
   
}






