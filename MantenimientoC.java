import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MantenimientoC implements ActionListener 
{
   JFrame vMantenimiento;

   private JLabel lbl_cedula, lbl_nombre, lbl_apellido, lbl_provincia, lbl_direccion, lbl_telefono, lbl_compra_anual;
   private JTextField tf_cedula, tf_nombre, tf_apellido, tf_provincia, tf_direccion, tf_telefono, tf_compra_anual, tf_limpiar;
   
   JButton btn_eliminar, btn_buscar, btn_agregar, btn_modificar, btn_limpiar;
   
   JComboBox<String> jcb_provincia;
   
   ListSelectionModel lsm;
   
   Cliente cliente;
   Provincia provincia;
    
   MantenimientoC(JFrame x)
   {
      vMantenimiento = x;
      vMantenimiento.getContentPane().removeAll();
      vMantenimiento.getContentPane().setBackground(Color.gray);
      vMantenimiento.revalidate();
       
       cliente = new Cliente();
       provincia = new Provincia();
       
       btn_buscar = new JButton("Buscar");
       btn_buscar.setBounds(225,50,100,20);
       btn_buscar.addActionListener(this);
       vMantenimiento.add(btn_buscar);
       
       btn_agregar = new JButton("agregar");
       btn_agregar.setBounds(225,75,100,20);
       btn_agregar.addActionListener(this);
       vMantenimiento.add(btn_agregar);
       
       btn_eliminar = new JButton("eliminar");
       btn_eliminar.setBounds(225,100,100,20);
       btn_eliminar.addActionListener(this);
       vMantenimiento.add(btn_eliminar);
       
       btn_modificar = new JButton("modificar");
       btn_modificar.setBounds(225,125,100,20);
       btn_modificar.addActionListener(this);
       vMantenimiento.add(btn_modificar);
       
       btn_limpiar = new JButton("limpiar");
       btn_limpiar.setBounds(225,150,100,20);
       btn_limpiar.addActionListener(this);
       vMantenimiento.add(btn_limpiar);
      
       lbl_cedula = new JLabel("Cedula");
       lbl_cedula.setBounds(50,50,80,20);
       vMantenimiento.add(lbl_cedula);
      
       tf_cedula = new JTextField();
       tf_cedula.setBounds(125,50,80,20);
       vMantenimiento.add(tf_cedula);
      
       lbl_nombre = new JLabel("Nombre");
       lbl_nombre.setBounds(50,75,80,20);
       vMantenimiento.add(lbl_nombre);
      
       tf_nombre = new JTextField();
       tf_nombre.setBounds(125,75,80,20);
       vMantenimiento.add(tf_nombre);
   
       lbl_apellido = new JLabel("Apellido");
       lbl_apellido.setBounds(50,100,80,20);
       vMantenimiento.add(lbl_apellido);
      
       tf_apellido = new JTextField();
       tf_apellido.setBounds(125,100,80,20);
       vMantenimiento.add(tf_apellido);
       
       lbl_direccion = new JLabel("Direccion");
       lbl_direccion.setBounds(50,125,80,20);
       vMantenimiento.add(lbl_direccion);
      
       tf_direccion = new JTextField();
       tf_direccion.setBounds(125,125,80,20);
       vMantenimiento.add(tf_direccion);
      
       lbl_telefono = new JLabel("Telefono");
       lbl_telefono.setBounds(50,150,80,20);
       vMantenimiento.add(lbl_telefono);
      
       tf_telefono = new JTextField();
       tf_telefono.setBounds(125,150,80,20);
       vMantenimiento.add(tf_telefono);
       
       lbl_compra_anual = new JLabel("Compra Anual");
       lbl_compra_anual.setBounds(50,175,80,20);
       vMantenimiento.add(lbl_compra_anual);
      
       tf_compra_anual = new JTextField();
       tf_compra_anual.setBounds(150,175,80,20);
       vMantenimiento.add(tf_compra_anual);
       
       lbl_provincia = new JLabel("Provincia");
       lbl_provincia.setBounds(50,200,80,20);
       vMantenimiento.add(lbl_provincia);
      
       jcb_provincia = new JComboBox<String>(provincia.vleer());
       jcb_provincia.setBounds(125,200,120,20);
       vMantenimiento.add(jcb_provincia);
       
       vMantenimiento.repaint();
   }
   
public void actionPerformed(ActionEvent e)
   {
     if (e.getSource() == btn_buscar)
     {
         System.out.println("BUSCADO");
         cliente.buscar(tf_cedula.getText());
         tf_nombre.setText(cliente.getNombre());
         tf_apellido.setText(cliente.getApellido());
         tf_direccion.setText(cliente.getDireccion());
         tf_telefono.setText(cliente.getTelefono());
         tf_compra_anual.setText(String.valueOf(cliente.getCompra_anual()));
     }
    
     if(e.getSource() == btn_agregar)
     {
         System.out.println("AGREGADO");
         cliente.setCedula(tf_cedula.getText());
         cliente.setNombre(tf_nombre.getText());
         cliente.setApellido(tf_apellido.getText());
         cliente.setDireccion(tf_direccion.getText());
         cliente.setTelefono(tf_telefono.getText());
         cliente.setCompra_anual(Integer.parseInt(tf_compra_anual.getText()));
         cliente.agregar();
     }
    
     if(e.getSource() == btn_eliminar)
         {
             System.out.println("ELIMADO");
             cliente.setCedula(tf_cedula.getText());
             cliente.setNombre(tf_nombre.getText());
             cliente.setApellido(tf_apellido.getText());
             cliente.setDireccion(tf_direccion.getText());
             cliente.setTelefono(tf_telefono.getText());
             cliente.setCompra_anual(Integer.parseInt(tf_compra_anual.getText()));
             cliente.eliminar();
         }
            
     if(e.getSource() == btn_modificar)
     {
         System.out.println("MODIFICADO");
         cliente.setCedula(tf_cedula.getText());
         cliente.setNombre(tf_nombre.getText());
         cliente.setApellido(tf_apellido.getText());
         cliente.setDireccion(tf_direccion.getText());
         cliente.setTelefono(tf_telefono.getText());
         cliente.setCompra_anual(Integer.parseInt(tf_compra_anual.getText()));
         cliente.modificar();
     } 
    
    if (e.getSource() == btn_limpiar)
    {
        System.out.println("LIMPIADO");
        tf_nombre.setText("");
        tf_cedula.setText("");
        tf_apellido.setText("");
        tf_telefono.setText("");
        tf_direccion.setText("");
        tf_compra_anual.setText("");
    }
           
   }
   
}